import * as React from 'react';
import Container from '@mui/material/Container';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';
import ProTip from '../src/ProTip';
import Link from '../src/Link';
import Copyright from '../src/Copyright';
import {useEffect, useState} from "react";
import { EventSourcePolyfill } from 'event-source-polyfill';
import Button from "@mui/material/Button";
import {Paper} from "@mui/material";
const token = "eyJhbGciOiJIUzI1NiJ9.eyJtZXJjdXJlIjp7InB1Ymxpc2giOlsiKiJdLCJzdWJzY3JpYmUiOlsiKiJdfX0.Ws4gtnaPtM-R2-z9DnH-laFu5lDZrMnmyTpfU8uKyQo"
const token_2 = "eyJhbGciOiJIUzI1NiJ9.eyJtZXJjdXJlIjp7InB1Ymxpc2giOlsiKiJdLCJzdWJzY3JpYmUiOlsiaHR0cHM6Ly9sb2NhbGhvc3QvYm9va3MiXX19.hiK1IqhVnnbaWTFBilnG2vqeWu937sxViE3AhJZChr4"
const token_3 = "eyJhbGciOiJIUzI1NiJ9.eyJtZXJjdXJlIjp7InB1Ymxpc2giOlsiKiJdLCJzdWJzY3JpYmUiOlsiaHR0cHM6Ly9sb2NhbGhvc3Qvc3NzcyIsImh0dHBzOi8vbG9jYWxob3N0L3Z2dnYiXX19.wOKDo1Wd7tNwHPH3Kz-FaxtwGAVSjJGA-CDweAOh_68"
export default function Index() {
  const [notification,setNotification] = useState([])
  useEffect(
      ()=>{
        const url = new URL('http:localhost:8003/.well-known/mercure');
        url.searchParams.append('topic', 'https://localhost/books');
        url.searchParams.append('topic', 'https://localhost/my-pubic-books');
        const eventSource = new EventSourcePolyfill(url.toString(), {
              withCredentials: true,
              headers: { Authorization: `Bearer ${token_3}` }
          });
        eventSource.onmessage = e =>{
            notification.push(JSON.stringify(e))
            setNotification([...notification])
        }  // do something with the payload
      },[""]
  )
    // useEffect(
    //     ()=>{
    //         const url = new URL('https://alpha-abbas-papi.jamendo.com/.well-known/mercure');
    //         url.searchParams.append('topic', 'https://alpha-abbas-papi.jamendo.com/books');
    //         url.searchParams.append('topic', 'https://alpha-abbas-papi.jamendo.com/music/albums/{id}');
    //         url.searchParams.append('topic', 'https://alpha-abbas-papi.jamendo.com/music/albums/{464670}');
    //         const eventSource = new EventSource(url);
    //         eventSource.onmessage = e => console.log(e); // do something with the payload
    //     },[""]
    // )

    const triggerMercure = ()=>{
        const myHeaders = new Headers();
        myHeaders.append("Varnish-Disable", "true");
        //myHeaders.append("Authorization", "Basic d2luYW1wLWFydGlzdDoydVZBQmU5WUlFR1Y=");

        const requestOptions = {
            method: 'GET',
            headers: myHeaders,
            redirect: 'follow'
        };

        fetch("http://localhost/adrev/contracts?page=1", requestOptions)
            .then(result => console.log(result))
            .catch(error => console.log('error', error));
    }
  return (
    <Container maxWidth="lg">
        <Button onClick={triggerMercure}>vbbb</Button>
      <Box sx={{ my: 4 }}>
          {
              notification.length===0 && 'Pas de notification'
          }
          {
              notification.length>0 &&
              notification.map(
                  (value,index)=>(
                      <Paper key={index} sx={{ p:3 ,mb:3}}>
                      { value }
                  </Paper>)
              )
          }
        <ProTip />
        <Copyright />
      </Box>
    </Container>
  );
}
